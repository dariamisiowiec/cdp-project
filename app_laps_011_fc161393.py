# group 11
# Daria Misiowiec - 161393
# Maria Santos - 39536

from laps_011_fc161393 import *

gpx = GPXDocument("MaratonaAveiro2019.gpx")

print()

print("Reading track from file", gpx.getFileName())
track = gpx.getTrack()

print()

total_distance = track.totalDistance()
print("Track total distance =", round(total_distance, 1), "m")


total_time = Analyse.secondsToHoursMinSec(track.totalTime())
print("Track total time =", total_time)

print()

print("Extracting laps from the track")
lapExtractor = LapExtractor(track)
laps = lapExtractor.getAutoLapsByDistance()

print()

print("Pace of each lap: ")
for lap in laps:
   no = lap.getLapNumber()
   pace = Analyse.paceDecimalMinutesToMinSec(lap.averageSpeed())
   print('{:2d}'.format(no), pace)

print()

print("Plot the laps with numbers [2, 10, 21, 37, 42, 43]")
print()

print("Need to CLOSE THE GRAPH WINDOW manually")
print()

plot = Plot()
for number in [2, 10, 21, 37, 42, 43]:
    data = laps[number-1].produceSeries("distance series", "pace")
    data = Analyse.filterSeries(data, 5)
    plot.add(data, False, number)
plot.show()

print("----- End of execution -----")